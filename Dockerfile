FROM openjdk
MAINTAINER Mohammad Khatib <mohmmad.mustafa@progressoft.com>

ARG File=*.jar

COPY target/$File /app.jar/
ENV ACTIVE=h2

CMD ["java", "-jar" , "-Dserver.port=8090", "-Dspring.profiles.active=${ACTIVE}", "app.jar"]

